#pragma once
#include <vector>
#include <cassert>
#include <iostream>

class BowlingGame
{
public:
    void roll(int pins) {
        _rolls.emplace_back(pins);
    }

    int score() {
        int score_value = 0;
        int roll = 0;
        for (int round = 0; round < 10; ++round) {
            if (10 == _rolls[roll]) {
                // STRIKE
                score_value += _rolls[roll] + _rolls[roll + 1] + _rolls[roll + 2];
                ++roll;
            }
            else if (10 == (_rolls[roll] + _rolls[roll + 1])) {
                // SPARE
                score_value += _rolls[roll] + _rolls[roll + 1] + _rolls[roll + 2];
                roll+=2;
            }
            else {
                // REGULAR round
                score_value += _rolls[roll] + _rolls[roll + 1];
                roll+=2;
            }
        }
        return score_value;
    }
private:
    std::vector<int> _rolls;
};
