#include "catch/catch.hpp"
#include "BowlingGame.h"

TEST_CASE("Simple_Game_1_all_the_time") {
    BowlingGame testObj;

    testObj.roll(1);
    testObj.roll(1);

    testObj.roll(1);
    testObj.roll(1);

    testObj.roll(1);
    testObj.roll(1);

    testObj.roll(1);
    testObj.roll(1);

    testObj.roll(1);
    testObj.roll(1);

    testObj.roll(1);
    testObj.roll(1);

    testObj.roll(1);
    testObj.roll(1);

    testObj.roll(1);
    testObj.roll(1);

    testObj.roll(1);
    testObj.roll(1);

    testObj.roll(1);
    testObj.roll(1);

    REQUIRE(20 == testObj.score());
}

TEST_CASE("Spare_at_begining") {
    BowlingGame testObj;

    testObj.roll(1);
    testObj.roll(9);

    testObj.roll(1);
    testObj.roll(1);

    testObj.roll(1);
    testObj.roll(1);

    testObj.roll(1);
    testObj.roll(1);

    testObj.roll(1);
    testObj.roll(1);

    testObj.roll(1);
    testObj.roll(1);

    testObj.roll(1);
    testObj.roll(1);

    testObj.roll(1);
    testObj.roll(1);

    testObj.roll(1);
    testObj.roll(1);

    testObj.roll(1);
    testObj.roll(1);

    REQUIRE(29 == testObj.score());
}

TEST_CASE("Spare_at_begining_and_stike_in_middle") {
    BowlingGame testObj;

    testObj.roll(1);
    testObj.roll(9);

    testObj.roll(1);
    testObj.roll(1);

    testObj.roll(1);
    testObj.roll(1);

    testObj.roll(1);
    testObj.roll(1);

    testObj.roll(1);
    testObj.roll(1);

    testObj.roll(10);

    testObj.roll(1);
    testObj.roll(1);

    testObj.roll(1);
    testObj.roll(1);

    testObj.roll(1);
    testObj.roll(1);

    testObj.roll(1);
    testObj.roll(1);

    REQUIRE(39 == testObj.score());
}

TEST_CASE("Perfect_Game") {
    BowlingGame testObj;

    testObj.roll(10);

    testObj.roll(10);

    testObj.roll(10);

    testObj.roll(10);

    testObj.roll(10);

    testObj.roll(10);

    testObj.roll(10);

    testObj.roll(10);

    testObj.roll(10);

    testObj.roll(10);
    testObj.roll(10);
    testObj.roll(10);

    REQUIRE(300 == testObj.score());
}

TEST_CASE("9_and_spare_all_the_time") {
    BowlingGame testObj;

    testObj.roll(9);
    testObj.roll(1);

    testObj.roll(9);
    testObj.roll(1);

    testObj.roll(9);
    testObj.roll(1);

    testObj.roll(9);
    testObj.roll(1);

    testObj.roll(9);
    testObj.roll(1);

    testObj.roll(9);
    testObj.roll(1);

    testObj.roll(9);
    testObj.roll(1);

    testObj.roll(9);
    testObj.roll(1);

    testObj.roll(9);
    testObj.roll(1);

    testObj.roll(9);
    testObj.roll(1);
    testObj.roll(9);

    REQUIRE(190 == testObj.score());
}

TEST_CASE("Random_game") {
    BowlingGame testObj;

    testObj.roll(9);
    testObj.roll(1);

    testObj.roll(2);
    testObj.roll(3);

    testObj.roll(10);

    testObj.roll(9);
    testObj.roll(0);

    testObj.roll(0);
    testObj.roll(10);

    testObj.roll(5);
    testObj.roll(3);

    testObj.roll(0);
    testObj.roll(0);

    testObj.roll(9);
    testObj.roll(1);

    testObj.roll(9);
    testObj.roll(0);

    testObj.roll(10);
    testObj.roll(10);
    testObj.roll(10);

    REQUIRE(126 == testObj.score());
}
